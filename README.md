# [WikiTribune Multi-lingual](https://www.wikitribune.com)

Multi-lingual repo containing the .PO files and .POT file to translate the WikiTribune site

## Process
__Method 1 - Poedit__
1. Open the Theme translation file `wt.pot` via software such as [Poedit](https://poedit.net/) `File >> New From POT/PO file` (note there are two plugin with translations `wiki-post-types` and `wiki-user-management` each with their own `.pot` translation file)
2. Chose locale (e.g. es_ES.po, fr_FR.po)
3. Translate
4. Save
5. Commit back to this repo

__Method 2 - Text editor__
1. Duplicate en_GB.po
2. Rename file to name_of_locale.po (e.g. fr_FR.po, de_DE.po)
3. Open via text editor
4. Add Translations and Save file
5. Commit back to this repo

## Missing translations
* Contact [info@wikitribune.com](info@wikitribune.com) so we can update the POT file, your own translation files would need to be updated and compiled. PO files can be edited in a text editor or software such as Poedit.

## Locales

[via Oracle - Supported Locales](https://www.oracle.com/technetwork/java/javase/javase7locales-334809.html)

| Language | Country | Locale ID |
| -- | -- | -- |
| Albanian | Albania | sq_AL |
| Arabic | Algeria | ar_DZ |
| Arabic | Bahrain | ar_BH |
| Arabic | Egypt | ar_EG |
| Arabic | Iraq | ar_IQ |
| Arabic | Jordan | ar_JO |
| Arabic | Kuwait | ar_KW |
| Arabic | Lebanon | ar_LB |
| Arabic | Libya | ar_LY |
| Arabic | Morocco | ar_MA |
| Arabic | Oman | ar_OM |
| Arabic | Qatar | ar_QA |
| Arabic | Saudi Arabia | ar_SA |
| Arabic | Sudan | ar_SD |
| Arabic | Syria | ar_SY |
| Arabic | Tunisia | ar_TN |
| Arabic | United Arab Emirates | ar_AE |
| Arabic | Yemen | ar_YE |
| Belarusian | Belarus | be_BY |
| Bulgarian | Bulgaria | bg_BG |
| Catalan | Spain | ca_ES |
| Chinese (Simplified) | China | zh_CN |
| Chinese (Simplified) | Singapore | zh_SG |
| Chinese (Traditional) | Hong Kong | zh_HK |
| Chinese (Traditional) | Taiwan | zh_TW |
| Croatian | Croatia | hr_HR |
| Czech | Czech Republic | cs_CZ |
| Danish | Denmark | da_DK |
| Dutch | Belgium | nl_BE |
| Dutch | Netherlands | nl_NL |
| English | Australia | en_AU |
| English | Canada | en_CA |
| English | India | en_IN |
| English | Ireland | en_IE |
| English | Malta | en_MT |
| English | New Zealand | en_NZ |
| English | Philippines | en_PH |
| English | Singapore | en_SG |
| English | South Africa | en_ZA |
| English | United Kingdom | en_GB |
| English | United States | en_US |
| Estonian | Estonia | et_EE |
| Finnish | Finland | fi_FI |
| French | Belgium | fr_BE |
| French | Canada | fr_CA |
| French | France | fr_FR |
| French | Luxembourg | fr_LU |
| French | Switzerland | fr_CH |
| German | Austria | de_AT |
| German | Germany | de_DE |
| German | Luxembourg | de_LU |
| German | Switzerland | de_CH |
| Greek | Cyprus | el_CY |
| Greek | Greece | el_GR |
| Hebrew | Israel | iw_IL |
| Hindi | India | hi_IN |
| Hungarian | Hungary | hu_HU |
| Icelandic | Iceland | is_IS |
| Indonesian | Indonesia | in_ID |
| Irish | Ireland | ga_IE |
| Italian | Italy | it_IT |
| Italian | Switzerland | it_CH |
| Japanese (Gregorian calendar) | Japan | ja_JP |
| Japanese (Imperial calendar) | Japan | ja_JP_JP |
| Korean | South Korea | ko_KR |
| Latvian | Latvia | lv_LV |
| Lithuanian | Lithuania | lt_LT |
| Macedonian | Macedonia | mk_MK |
| Malay | Malaysia | ms_MY |
| Maltese | Malta | mt_MT |
| Norwegian (Bokmål) | Norway | no_NO |
| Norwegian (Nynorsk) | Norway | no_NO_NY |
| Polish | Poland | pl_PL |
| Portuguese | Brazil | pt_BR |
| Portuguese | Portugal | pt_PT |
| Romanian | Romania | ro_RO |
| Russian | Russia | ru_RU |
| Serbian (Cyrillic) | Bosnia and Herzegovina | sr_BA |
| Serbian (Cyrillic) | Montenegro | sr_ME |
| Serbian (Cyrillic) | Serbia | sr_RS |
| Serbian (Latin) | Bosnia and Herzegovina | sr_Latn_BA |
| Serbian (Latin) | Montenegro | sr_Latn_ME |
| Serbian (Latin) | Serbia | sr_Latn_RS |
| Slovak | Slovakia | sk_SK |
| Slovenian | Slovenia | sl_SI |
| Spanish | Argentina | es_AR |
| Spanish | Bolivia | es_BO |
| Spanish | Chile | es_CL |
| Spanish | Colombia | es_CO |
| Spanish | Costa Rica | es_CR |
| Spanish | Dominican Republic | es_DO |
| Spanish | Ecuador | es_EC |
| Spanish | El Salvador | es_SV |
| Spanish | Guatemala | es_GT |
| Spanish | Honduras | es_HN |
| Spanish | Mexico | es_MX |
| Spanish | Nicaragua | es_NI |
| Spanish | Panama | es_PA |
| Spanish | Paraguay | es_PY |
| Spanish | Peru | es_PE |
| Spanish | Puerto Rico | es_PR |
| Spanish | Spain | es_ES |
| Spanish | United States | es_US |
| Spanish | Uruguay | es_UY |
| Spanish | Venezuela | es_VE |
| Swedish | Sweden | sv_SE |
| Thai (Western digits) | Thailand | th_TH |
| Thai (Thai digits) | Thailand | th_TH_TH |
| Turkish | Turkey | tr_TR |
| Ukrainian | Ukraine | uk_UA |
| Vietnamese | Vietnam | vi_VN |
